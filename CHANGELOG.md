<a name="0.2"></a>
### Bug Fixes
* Set cooldown after probing setting False by default
* Do not cooldown on direct G29 gcode command, ie only if mesh leveling process is launched from control button
* Fix error in settings on Chromium browser as @blondak mentionned
